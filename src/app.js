const express = require('express');
const { engine } = require('express-handlebars');
const myconnection = require('express-myconnection');
const mysql = require('mysql');
const session = require('express-session');
const bodyParser = require('body-parser');

const loginRoutes = require('./routes/login');
const tasksRoutes = require('./routes/tasks');

const app = express();
app.set ('port', 4000);


app.set('views', __dirname + '/views');
app.engine('.hbs', engine({
    extname: '.hbs',
}));
app.set('view engine', 'hbs');


app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

app.use(myconnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '',
    port: 3306,
    database: 'nodelogin'

}, 'single'));

app.use(express.static('public'));

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

app.listen(app.get('port'), () => {
    console.log('Listening on port ', app.get('port'));
});

app.use('/', loginRoutes);
app.use('/', tasksRoutes);

app.use((req, res, next) => {
    // Este middleware se ejecutará en cada solicitud
    // Agrega el objeto { name: req.session.name } a res.locals
    res.locals.name = req.session.name;
    next();
});

app.get('/' , (req, res) => {
    if(req.session.loggedin == true){

        res.render('home', { name: req.session.name });

    }else{
        res.redirect('/login');
    }
});

app.get('/tasks/edit', (req, res) => {
    if (req.session.loggedin == true) {
        res.render('tasks/edit', { name: req.sessions.name });
    } else {
        res.redirect('/login');
    }
});

