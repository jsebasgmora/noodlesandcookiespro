const express = require('express');
const tasksController = require('../controllers/taskscontroller');

const router = express.Router();

router.get('/tasks/edit', tasksController.edit);
router.get('/tasks/search', tasksController.search);
router.get('/tasks/create', tasksController.create);
router.get('/tasks/edition', tasksController.edition);
router.post('/tasks/create', tasksController.store);
router.post('/tasks/search', tasksController.searcher);

module.exports = router; 