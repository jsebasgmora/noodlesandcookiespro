/*function store(req, res){
    if(req.session.loggedin == true){
         const data = req.body;
         const name = req.session.name;
         

            req.getConnection((err, conn) => {
                conn.query('INSERT INTO products SET ?', [data], (err, rows) => {
                    
                    res.render('tasks/create', { name: req.session.name });
                });
            });
    }
}       */
function searcher(req, res) {
    if(req.session.loggedin == true){
    const { category, product_name, minPrice, maxPrice, priceFilter, minStock, maxStock } = req.body;

    let query = 'SELECT * FROM products WHERE 1 = 1'; // WHERE 1=1 es una condición siempre verdadera para facilitar la construcción de la consulta

    // Añadir condiciones según los filtros proporcionados
    if (category && product_name) {
        // Si ambos category y product_name están presentes, se filtra por ambos
        query += ` AND category = '${category}' AND (product_name LIKE '%${product_name}%')`;
    } else if (category) {
        // Si solo hay category, se filtra solo por category
        query += ` AND category = '${category}'`;
    } else if (product_name) {
        // Si solo hay product_name, se filtra solo por product_name
        query += ` AND (product_name LIKE '%${product_name}%' OR category = '${product_name}')`;
    }

    if (minPrice && maxPrice) {
        query += ` AND price BETWEEN ${minPrice} AND ${maxPrice}`;
    } else if (minPrice) {
        query += ` AND price >= ${minPrice}`;
    } else if (maxPrice) {
        query += ` AND price <= ${maxPrice}`;
    }

    if (priceFilter) {
        query += ` ORDER BY price ${priceFilter === 'mayor' ? 'DESC' : 'ASC'}`;
    }

    if (minStock && maxStock) {
        query += ` AND stock BETWEEN ${minStock} AND ${maxStock}`;
    } else if (minStock) {
        query += ` AND stock >= ${minStock}`;
    } else if (maxStock) {
        query += ` AND stock <= ${maxStock}`;
    }

    req.getConnection((err, conn) => {
        conn.query(query, (err, products) => {
            if (err) {
                res.json(err);
            }
            const name = req.session.name;
            res.render('tasks/search', { products, name: req.session.name });
        });
    });
        }
    else{
        res.redirect('/');
    }
}



function store(req, res){
    
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO products SET ?', [data], (err, rows) => {
            res.redirect('/tasks/edit');
        }); 
    });    
}

function edition(req, res){
    if(req.session.loggedin == true){

        const name = req.session.name;
        res.render('tasks/edition', { name: req.session.name });

    }else{
        res.redirect('/');
    }
    
}

function create(req, res){
    if(req.session.loggedin == true){

        const name = req.session.name;
        res.render('tasks/create', { name: req.session.name });

    }else{
        res.redirect('/');
    }
    
}

function edit(req, res){
    if(req.session.loggedin == true){

        const name = req.session.name;        
        res.render('tasks/edit', { name: req.session.name });

    }else{
        res.redirect('/');
    }
    
}

function search(req, res){
    if(req.session.loggedin == true){

        const name = req.session.name;  
        res.render('tasks/search', { name: req.session.name });

    }else{
        res.redirect('/');
    }
    
}
/*
function auth(req, res){
    const data = req.body;

    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM users WHERE email = ?', [data.email], (err, userdata) => {
           
            if(userdata.length>0){
                userdata.forEach(element => {
                    bcrypt.compare(data.password, element.password, (err, isMatch) => {
                    

                        if(!isMatch){
                            res.render('login/index', {error: 'Error: incorrect password !'});
                        }else{
                            
                            req.session.loggedin = true;
                            req.session.name = element.name;

                            res.redirect('/');
                        }
                                                
                    });

                    

                })

            }else{
                res.render('login/index', {error: 'Error: user not exists !'});
            }
        });
    });
}

function register(req, res){
    if(req.session.loggedin != true){

        res.render('login/register');

    }else{
        res.redirect('/');
    }
}

function storeUser(req, res) {
    const data = req.body;

    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM users WHERE email = ?', [data.email], (err, userdata) => {
            if(userdata.length>0){
                res.render('login/register', {error: 'Error: user already exists !'});               
            }else{

                bcrypt.hash(data.password, 12).then(hash => {
                    data.password = hash;
                    req.getConnection((err, conn) => {
                        conn.query('INSERT INTO users SET ?', [data], (err, rows) => {
                            res.redirect('/');
                        });
                    });
                });                
            }
        });
    });  
}


function logout(req, res){
    if(req.session.loggedin == true){

        req.session.destroy();

    }
    res.redirect('/login');

}
*/





module.exports = {
    
    edit,
    search,
    create,
    edition,
    store,
    searcher,
}